local groupbuilder = require("groupbuilder.groupbuilder")
local unitbuilder = require("groupbuilder.unitbuilder")

local TrySpawn = function(unitTypeList, position)
        local units = {}
        for _,t in pairs(unitTypeList) do
                table.insert(units, unitbuilder.New(t))
        end
        local group = groupbuilder.New("SpawnedGroup"):withCountry("CJTF_RED")
        local surfaceType = land.getSurfaceType(position)
        if (surfaceType == 2 or surfaceType == 3) then group.category = Group.Category.SHIP end
        group = group:withUnits(units):atPosition(position)
        local spawn = group:spawn()
        if spawn == nil then trigger.action.outText("Failed to spawn group with unit " .. unitName) end
        return spawn
end

local MarkEvent = function(event)
        local mark = {
                idx = event.idx,
                time = event.time,
                creator = event.initiator,
                coalition = event.coalition,
                groupId = event.groupID,
                text = event.text,
                position = event.pos
        }
        return mark
end

MarkerHandler = {}
MarkerHandler.ActiveMarks = {}
MarkerHandler.HandleNewMark = function(event)


        if not (event.id == world.event.S_EVENT_MARK_CHANGE) then return end

        local mark = MarkEvent(event)
        local activeMarkSpawn = MarkerHandler.ActiveMarks[mark.idx]
        if(activeMarkSpawn ~= nil) then 
                local g = Group.getByName(activeMarkSpawn.name)
                if g ~= nil and g.destroy ~= nil then g:destroy() end
                MarkerHandler.ActiveMarks[mark.idx] = nil
        end

        local position = mist.utils.makeVec2(mark.position)
        units = {}
        for s in mark.text:gmatch("[^\r\n]+") do
                table.insert(units, s)
        end
        local spawned = TrySpawn(units, position)
        MarkerHandler.ActiveMarks[mark.idx] = spawned

        mist.scheduleFunction(function()
                if spawned == nil then return end
                local grp = Group.getByName(spawned.name)
                local ctrl = grp:getController()
                ctrl:setOnOff(false)
        end, {}, timer.getTime() + 1)
end


mist.addEventHandler(MarkerHandler.HandleNewMark)
trigger.action.outText("Marker Handler Loaded", 5)
return MarkerHandler;